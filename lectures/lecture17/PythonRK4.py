# Use RK4 to Solve y'' = -sin(y), y(0)=1, y'(0) = 0

#import numpy module
import numpy as np

#-------------
def rK4N(x,y,hs):
    #RK4 advancement of ODEs dy/dx = f(x,y) defined by RHS

    deltay = np.zeros_like(y)
    k1 = RHS(y)

    deltay = 0.5*hs*k1
    k2 = RHS(y+deltay)

    deltay = 0.5*hs*k2
    k3 = RHS(y+deltay)

    deltay = hs*k3
    k4 = RHS(y+deltay)
    x = x + hs
    y = y + (hs/6.)*(k1 + 2*(k2+k3) + k4)

    return x,y
#-------------
def RHS(y):
    #defines RHS of ODE, called by rk4N
    dy = np.zeros_like(y)
    dy[0] = y[1]
    dy[1] = -np.sin(y[0])
    return dy
#-------------

N=20 # time steps
h=0.5 # step size
x = 0. # initial time
y = np.array([1.0,0]) # initial condition

print("     x          y(x)       dy(x)")
print("{0:10.4f}".format(x),"{0:16.8f}".format(y[0]),"{0:16.8f}".format(y[1]))
#march forward in time
for i in range (N):
    x,y = rK4N(x,y, h)
    print("{0:16.8f}".format(x),"{0:16.8f}".format(y[0]),"{0:16.8f}".format(y[1]))
